package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    private String poste;
    private final String DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
    private String salle;
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        poste = "";
        salle = DISTANCIEL;
        model = new SuiviViewModel(getActivity().getApplication());

        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(), R.array.list_salles, android.R.layout.list_content);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spSalle.setAdapter(adapter);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            TextView textUsername = getActivity().findViewById(R.id.tvLogin);
            String username = textUsername.getText().toString();
            model.setUsername(username);
            Toast.makeText(this.getContext(), username, Toast.LENGTH_LONG).show();

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        update();
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                salle = ((Spinner) getActivity().findViewById(R.id.spSalle)).toString();
                update();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                poste = ((Spinner) getActivity().findViewById(R.id.spPoste)).toString();
                update();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        // TODO Q9
    }

    public void update(){
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        if( DISTANCIEL.equals(this.salle)) {
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
        } else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(this.poste + " : " + this.salle);
        }
    }
    // TODO Q9
}